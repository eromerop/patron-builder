package factorys;

import abstracts.Calentador;
import abstracts.Lampara;

public interface FabricaHerramientas{

    Lampara createLampara(String tamano, String pilas, int nivel);

    Calentador createCalentador(String modelo, String capacidad, int tubos);

}
